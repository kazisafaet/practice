
public class PalindromeCheck {

	public static void main(String[] args) {
		String s = "0";
		int n = s.length();
		
		String str = "";
		
		for(int i=n-1;i>=0;i--){
			str = str + s.charAt(i);
		}
		if(str.equals(s)) System.out.println("P");
		else System.out.println("NP");
	}
}
