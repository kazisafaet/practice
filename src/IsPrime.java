
public class IsPrime {

	public static void main(String[] args) {
		int key = 13;
		boolean isPrime = true;
		
		for(int i=3; i<key; i++){
			if(key%i==0){
				isPrime = false;
				break;
			}
		}
		if(isPrime) System.out.println(key+" is a Prime");
		else System.out.println(key+" is not a Prime");

	}

}
